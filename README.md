***Logistic Regression***

**Question1** 

Load packages, libraries, file.

**Question 2** 

Estimate the model 1 and evaluate the indicativity of each coefficient : ln(p/(1-p))= B0 + B1GEST + B2DIALTE+...+BpGEMEL.
p= P (premature= positive).
H0 : the birth isn't prematured.
H1 : The birth is prematured.


**Question 3** 

Select automatically the variables with the step function and create a model 2.
 

**Question 4** 

Which of the variables are the risk factors for having a prematured birth ? And which of them are the protective factors ?
Reminder : OR(Xj)= exp(Bj).
p-values with Wald test.
H0 : Bj=0 (the variable isn't indicative).
H1 : Bj different from 0 (the variable is indicative in the model).

**Question 5** 

Calculate the predicted values in the interest probabilities (predictive score).
Visualize and comment the quality of prediction.
Reminder : pi hat i (Xi) = P(Y= 1 | X= xi; B hat)= e B0 hat, B1X1i hat + B2X2i.
hat + /// / 1+ e B0 hat + B1X1i hat + B2X2i hat +...

**Question 6** 

Calculate the number of good sorted.
Ys hat = 1 if P(Y= 1| X= x; Beta hat) >= s
Ys hat = 0 otherwise

**Question 7** 

Trace the ROC curve using package ROCR. Comment the UAC.

**BONUS** 

Analyze with cross validation methods.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2020

